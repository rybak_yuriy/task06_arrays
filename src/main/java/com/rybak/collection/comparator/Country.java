package com.rybak.collection.comparator;

import java.util.Comparator;

public class Country {
    private static final Comparator<Country> BY_COUNTRY_NAME =
            (Country c1, Country c2) -> c1.getCountryName().compareToIgnoreCase(c2.getCountryName());
    private static final Comparator<Country> BY_CAPITAL =
            (Country c1, Country c2) -> c1.getCapital().compareToIgnoreCase(c2.getCapital());
    private String countryName;
    private String capital;
    Country() {
    }

    Country(String countryName, String capital) {
        this.countryName = countryName;
        this.capital = capital;
    }

    static Comparator<Country> getByCountryName() {
        return BY_COUNTRY_NAME;
    }

    static Comparator<Country> getByCapital() {
        return BY_CAPITAL;
    }

    private String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }
}
