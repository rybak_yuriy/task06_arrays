package com.rybak.collection.comparator;

import java.util.*;

public class Main {
    public static Comparator<Country> byNameComparator = Country.getByCountryName();
    public static Comparator<Country> byCapitalComparator = Country.getByCapital();
    Country country = new Country();

    public static void main(String[] args) {

        List<Country> countries = new ArrayList<>();
        countries.add(new Country("Ukraine", "Kyiv"));
        countries.add(new Country("Norway", "Oslo"));
        countries.add(new Country("Italy", "Rome"));
        countries.add(new Country("Malta", "Valletta"));
        countries.add(new Country("Japan", "Tokyo"));

        Country[] countriesArray = countries.toArray(new Country[0]);

        System.out.println("<-------------Sorting in ArrayList-------------->");
        System.out.println("\t" + "-by country name");
        List<Country> sortedByName = Main.sortByCountryName(countries);
        sortedByName.forEach(System.out::println);
        System.out.println("\t" + "-by capital");
        List<Country> sortedByCapital = Main.sortByCapital(countries);
        sortedByCapital.forEach(System.out::println);

        System.out.println("<-------------Sorting in Array-------------->");
        System.out.println("\t" + "-by country name");
        Country[] sortByCountryNameArray = Main.sortByCountryNameInArray(countriesArray);
        for (Country c : sortByCountryNameArray) {
            System.out.println(c);
        }
        System.out.println("\t" + "-by capital");
        Country[] sortByCapitalArray = Main.sortByCapitalInArray(countriesArray);
        for (Country c : sortByCapitalArray) {
            System.out.println(c);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please write capital to search in personList: ");
        String capital = scanner.nextLine();
        Country country = new Country();
        country.setCapital(capital);

        int index = Collections.binarySearch(countries, country, byCapitalComparator);
        System.out.println("Country with capital " + capital + " has a position: " + index);
    }


    public static List<Country> sortByCountryName(List<Country> countries) {
        Collections.sort(countries, byNameComparator);

        return countries;
    }

    public static Country[] sortByCountryNameInArray(Country[] countries) {
        Arrays.sort(countries, byNameComparator);
        return countries;
    }


    public static List<Country> sortByCapital(List<Country> countries) {
        Collections.sort(countries, byCapitalComparator);
        return countries;
    }

    public static Country[] sortByCapitalInArray(Country[] countries) {
        Arrays.sort(countries, byCapitalComparator);
        return countries;
    }
}