package com.rybak.collection.deque;

import java.util.*;

public class MyDeque<E> extends AbstractCollection<E> implements Deque<E> {

    private int capacity = 10;
    private E[] queue;
    private int size;
    private int head;
    private int tail;

    public MyDeque() {
        this.head = -1;
        this.size = 0;
        this.head = 0;
        this.tail = 0;
        queue = (E[]) new Object[capacity];

    }


    @Override
    public void addFirst(E e) {
        checkCapacity();
        if (head == -1) {
            head = 0;
        } else if (head == 0) {
            head = capacity - 1;
        } else {
            head -= 1;
        }
        queue[head] = e;
        size++;

    }

    private void checkCapacity() {
        if (size == capacity) {
            ensureCapacity();
        }
    }

    private void ensureCapacity() {
        int headLength = queue.length - head;
        int tailLength = tail + 1;
        capacity += capacity >> 1;
        Object[] newQueue = new Object[capacity];
        System.arraycopy(queue, head, newQueue, capacity - headLength, headLength);
        System.arraycopy(queue, 0, newQueue, 0, tailLength);
        queue = (E[]) newQueue;
        head = capacity - headLength;
    }

    @Override
    public void addLast(E e) {
        checkCapacity();
        if (head == -1) {
            tail = 0;
        }
        if (tail == capacity - 1) {
            tail = 0;
        } else {
            tail = tail + 1;
        }
        queue[tail] = e;
        size++;
    }

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return queue[head].equals(e);
    }

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return queue[tail].equals(e);
    }

    private void grow() {
        int oldCapacity = queue.length;
        int newCapacity = (int) (oldCapacity * 1.5 + 1);
        queue = Arrays.copyOf(queue, newCapacity);
    }

    @Override
    public E removeFirst() {
        return null;
    }

    @Override
    public E removeLast() {
        return null;
    }

    @Override
    public E pollFirst() {
        return null;
    }

    @Override
    public E pollLast() {
        return null;
    }

    @Override
    public E getFirst() {
        return (E) queue[head];
    }

    @Override
    public E getLast() {
        return (E) queue[tail];
    }

    @Override
    public E peekFirst() {
        return null;
    }

    @Override
    public E peekLast() {
        return null;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public E remove() {
        return null;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    @Override
    public void push(Object o) {

    }

    @Override
    public E pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection collection) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection collection) {
        return false;
    }

    @Override
    public boolean containsAll(Collection collection) {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return new Object[0];
    }

    @Override
    public Iterator descendingIterator() {
        return null;
    }
    public static void main(String[] args) {
        MyDeque<Integer> deque = new MyDeque<>();
        deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);
        deque.addFirst(4);
        deque.addFirst(5);
        deque.addFirst(6);
        deque.addFirst(7);
        deque.addFirst(8);
        deque.addFirst(9);
        deque.addFirst(10);
        deque.addFirst(11);
        System.out.println("Capacity= " + deque.capacity);
        deque.addLast(12);
        deque.addLast(13);
        deque.addLast(14);
        deque.addLast(15);
        deque.addLast(16);
        System.out.println("Capacity= " + deque.capacity);
        System.out.println(deque.tail);
        System.out.println(deque.head);

    }
}
