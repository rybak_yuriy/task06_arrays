package com.rybak.collection.priorityqueue;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class PriorityQueue<E extends Comparable> extends AbstractQueue<E> {

    private int capacity = 10;
    private int size = 0;
    private E[] heap;

    public PriorityQueue() {
        heap = (E[]) new Comparable[capacity];
    }

    @Override
    public boolean add(E e) {
        if (offer(e))
            return true;
        else
            throw new IllegalStateException("PriorityQueue is full");
    }

    private void moveUp() {
    }

    @Override
    public E remove() {
        return super.remove();
    }

    @Override
    public E element() {
        return super.element();
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return super.addAll(collection);
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        int i = size;
        if (i >= heap.length) {
            grow();
        }
        size++;
        if (i == 0) {
            heap[0] = e;
        } else {
            shiftUp(i, e);
        }
        return true;
    }

    private void shiftUp(int i, E e) {
    }

    private void grow() {
        int oldCapacity = heap.length;
        int newCapacity = (int) (oldCapacity * 1.5) + 1;
        heap = Arrays.copyOf(heap, newCapacity);
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    private void checkCapacity() {
        if (size == capacity) {
            capacity += capacity >> 1;
            heap = Arrays.copyOf(heap, capacity);
        }
    }
}
