package com.rybak.collection.stringcontainer;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        StringContainer stringContainer = new StringContainer();

        long startTime;
        long stopTime;
        System.out.println("<-----------------Tests of StringContainer------------->");
        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            stringContainer.addString(String.valueOf(i));
        }
        stopTime = System.currentTimeMillis();
        System.out.println("Time for adding 100_000 elements to StringContainer= "
                + (stopTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = 0; i < stringContainer.getSize(); i++) {
            stringContainer.getString(i);
        }
        stopTime = System.currentTimeMillis();
        System.out.println("Time for getting 100_000 elements from StringContainer= "
                + (stopTime - startTime));

        System.out.println("<-----------------Tests of ArrayList------------------>");
        List<String> list = new ArrayList<>();

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            list.add(String.valueOf(i));
        }
        stopTime = System.currentTimeMillis();
        System.out.println("Time for adding 100_000 elements to ArrayList= "
                + (stopTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            list.get(i);
        }
        stopTime = System.currentTimeMillis();
        System.out.println("Time for getting 100_000 elements from ArrayList= "
                + (stopTime - startTime));
    }


}
