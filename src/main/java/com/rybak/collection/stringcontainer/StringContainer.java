package com.rybak.collection.stringcontainer;

public class StringContainer {

    private String[] strings = new String[10];

    private int size;

    StringContainer() {
        size = 0;
    }

    public StringContainer(String[] strings) {
        this.strings = strings;
        size = strings.length;
    }


    String getString(int index) {
        if (index < size) {
            return strings[index];
        } else {
            return null;
        }
    }

    int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    void addString(String s) {
        if (size == strings.length) {
            enlargeCapacity();
        }
        this.strings[size++] = s;
    }

    private void enlargeCapacity() {
        String[] newStrings = new String[(int) (this.strings.length * 1.5)];
        for (int i = 0; i < strings.length; i++) {
            newStrings[i] = this.strings[i];
        }
        this.strings = newStrings;
    }
}
