package com.rybak.game;

import com.rybak.game.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().start();
    }
}
