package com.rybak.game.controller;

import com.rybak.game.model.GameArtifact;

public interface GameController {


    public void play(int door);
    int countDeadDoor(GameArtifact[] room);
    public void printRoom();
}
