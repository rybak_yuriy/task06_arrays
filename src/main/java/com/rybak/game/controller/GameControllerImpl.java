package com.rybak.game.controller;

import com.rybak.game.model.Game;
import com.rybak.game.model.GameArtifact;

public class GameControllerImpl implements GameController {

    Game game;

    public GameControllerImpl() {
        game = new Game();
    }

    @Override
    public void play(int door) {
        game.play(door);
    }

    @Override
    public int countDeadDoor(GameArtifact[] room) {
        return game.countDeadDoor(room);
    }

    @Override
    public void printRoom() {
        game.printRoom();
    }

    public GameArtifact[] getRoom() {
        return game.getRoom();
    }

    public void order(){
        game.orderDoors();
    }
}
