package com.rybak.game.model;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

public class Game {

    private Hero hero;
    private GameArtifact[] room = new GameArtifact[10];
    private Random random = new Random();
    private Scanner scanner = new Scanner(System.in);

    public Game() {
        hero = new Hero();
        for (int i = 0; i < room.length; i++) {
            room[i] = randArtifact();
        }
    }

    public void play(int door) {
        GameArtifact gameArtifact = chooseDoor(door);
        int heroPower = getCurrentHeroPower();

        while (gameArtifact instanceof MagicalArtifact) {
            heroPower = heroPower + gameArtifact.getPower();
            System.out.println("You are lucky, you've got additional power " + gameArtifact.getPower());
            System.out.println("Now you power is " + heroPower);
            ((MagicalArtifact) gameArtifact).setPower(0);
            System.out.println("Choose next door");
            int currPower = gameArtifact.getPower();
            if (currPower == 0) {
                System.out.println("You opened this door later");
                System.out.println("Choose another door");
                door = scanner.nextInt();
            }
            gameArtifact = chooseDoor(door);
        }

        if (gameArtifact instanceof Monster) {
            if (heroPower >= gameArtifact.getPower()) {
                System.out.println("Your power is " + heroPower);
                System.out.println("Monster power is " + gameArtifact.getPower());
                System.out.println("Congratulations, You win");
            } else {
                System.out.println("Your power is " + heroPower);
                System.out.println("Monster power is " + gameArtifact.getPower());
                System.out.println("Unfortunately, You lose");
            }
        }

    }

    private GameArtifact randArtifact() {
        int i = random.nextInt(2);
        if (i == 1) {
            return new MagicalArtifact();
        } else return new Monster();
    }

    public void printRoom() {
        for (int i = 0; i < room.length; i++) {
            GameArtifact g = room[i];
            System.out.println("Door # " + i + " | "
                    + g.getClass().getName().substring(21) + "Power= " + g.getPower());
        }
    }

    private GameArtifact chooseDoor(int i) {
        return room[i];
    }

    public int countDeadDoor(GameArtifact[] room) {
        int n = 0;

        for (GameArtifact gameArtifact : room) {
            if (gameArtifact instanceof Monster) {
                if (gameArtifact.getPower() > hero.getPower()) {
                    n++;
                }
            }
        }
        return n;
    }

    private int getCurrentHeroPower() {
        return hero.getPower();
    }

    public GameArtifact[] orderDoors() {

        System.out.println("It is the best order to open ");
        for (int i = 0; i < room.length; i++) {
            if ((room[i] instanceof Monster) && (room[i].getPower() <= 25))
                System.out.println("#" + i + " " + room[i]);
        }
        Arrays.sort(room, new SortByPower());
        return room;
    }

    public GameArtifact[] getRoom() {
        return room;
    }

    static class SortByPower implements Comparator<GameArtifact> {

        @Override
        public int compare(GameArtifact g1, GameArtifact g2) {
            return g1.getPower() - g2.getPower();
        }
    }
}
