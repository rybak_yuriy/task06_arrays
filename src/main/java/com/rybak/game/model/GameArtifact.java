package com.rybak.game.model;

public abstract class GameArtifact {

    int power;

    GameArtifact() {
    }

    public int getPower() {
        return power;
    }
}
