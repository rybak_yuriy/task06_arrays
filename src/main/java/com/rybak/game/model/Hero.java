package com.rybak.game.model;

public class Hero extends GameArtifact {

//    int power;

    public Hero() {
        power = 25;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "power=" + power +
                '}';
    }
}
