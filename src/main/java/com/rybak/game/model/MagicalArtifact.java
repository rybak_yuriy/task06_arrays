package com.rybak.game.model;

import java.util.Random;

public class MagicalArtifact extends GameArtifact {

    private final int min = 10;
    private final int max = 80;
    private Random random = new Random();

    public MagicalArtifact() {
        power = random.nextInt(max - min) + min;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "MagicalArtifact{" +
                "power=" + power +
                '}';
    }
}
