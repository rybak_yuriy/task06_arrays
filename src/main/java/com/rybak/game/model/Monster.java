package com.rybak.game.model;

import java.util.Random;

public class Monster extends GameArtifact {

    private Random random = new Random();
    private final int min=5;
    private final int max=100;

    public Monster() {
        power = random.nextInt(max-min)+min;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "power=" + power +
                '}';
    }
}
