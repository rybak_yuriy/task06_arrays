package com.rybak.game.view;

import com.rybak.game.controller.GameControllerImpl;

import java.util.Scanner;

public class MainView {
    GameControllerImpl gameController;
    private Scanner scanner = new Scanner(System.in);

    public MainView() {
        gameController = new GameControllerImpl();
    }

    public void start() {

        System.out.println("<_________________GAME STARTS_______________>");
        System.out.println("<---------------------ROOM------------------>");
        gameController.printRoom();
        System.out.println("<------------------------------------------->");
        int deadRoom = gameController.countDeadDoor(gameController.getRoom());
        System.out.println("Here are " + deadRoom + " doors where you can lose, be careful");
        System.out.println("Please enter the number of the door you want to open: ");
        int door = scanner.nextInt();
        gameController.play(door);
    }


}
