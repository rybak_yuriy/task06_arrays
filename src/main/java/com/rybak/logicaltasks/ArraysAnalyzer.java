package com.rybak.logicaltasks;

import java.util.Arrays;

public class ArraysAnalyzer {

    private static int findEndOfSequence(int[] array, int index) {
        if ((index > array.length) || (index < 0)) {
            return -1;
        }

        if (index == array.length -1) {
            return index;
        } else if (array[index] == array[index + 1]) {
            return findEndOfSequence(array, index + 1);
        } else {
            return index;
        }
    }

    public int[] removeDuplicates(int[] array) {
        int[] resultedArray = new int[array.length];
        int resultedArrayIndex = 0;

        int index = 0;
        while (index <= array.length - 1) {
            int endOfSequence = findEndOfSequence(array, index);
            resultedArray[resultedArrayIndex++] = array[index];
            index = endOfSequence + 1;
        }
        return Arrays.copyOf(resultedArray, resultedArrayIndex);
    }

    public int[] deleteMoreThenTwiceRepeat(int[] array) {
        int[] resultedArray = new int[array.length];
        int resultedArrayIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (numberOfAllOccurrences(array, array[i]) <= 2) {
                resultedArray[resultedArrayIndex++] = array[i];
            }
        }
        return Arrays.copyOf(resultedArray, resultedArrayIndex);
    }

    private static int numberOfAllOccurrences(int[] array, int valueToFind) {
        int occurrencesCount = 0;
        for (int i = 0; i < array.length; i++) {
            if (valueToFind == array[i]) {
                occurrencesCount++;
            }
        }
        return occurrencesCount;
    }
}
