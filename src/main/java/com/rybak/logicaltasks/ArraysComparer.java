package com.rybak.logicaltasks;

import java.util.Arrays;

public class ArraysComparer {


    private static boolean contains(int[] array, int value) {
        boolean result = false;

        for (int i : array) {
            if (i == value) {
                result = true;
                break;
            }
        }

        return result;
    }

    public int[] showCommonElements(int[] a, int[] b) {
        int[] commonArray = new int[Math.min(a.length, b.length)];
        int currIndex = 0;
        for (int value : a) {
            if (contains(b, value)) {
                commonArray[currIndex] = value;
                currIndex++;
            }
        }
        return Arrays.copyOf(commonArray, currIndex);
    }

    public int[] showUniqueElements(int[] a, int[] b) {
        int[] resultedArray = new int[a.length + b.length];
        int currIndex1 = 0;
        int currIndex2;
        for (int value : a) {
            if (!contains(b, value)) {
                resultedArray[currIndex1] = value;
                currIndex1++;
            }
        }
        currIndex2 = currIndex1;
        for (int value : b) {
            if (!contains(a, value)) {
                resultedArray[currIndex2] = value;
                currIndex2++;
            }
        }

        return Arrays.copyOf(resultedArray, currIndex2);
    }


}



