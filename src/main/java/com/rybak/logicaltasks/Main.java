package com.rybak.logicaltasks;

public class Main {

    public static void main(String[] args) {

        int[] a = new int[]{1, 2, 4, 5, 5, 5};
        int[] b = new int[]{1, 3, 6, 7, 8};

        ArraysAnalyzer analyzer = new ArraysAnalyzer();
        ArraysComparer comparer = new ArraysComparer();

        System.out.println("show unique elements of two arrays");
        int[] uniqueElements = comparer.showUniqueElements(a, b);
        for (int i : uniqueElements) {
            System.out.println(i);

        }

        System.out.println("show common elements of two arrays");
        int[] commonElements = comparer.showCommonElements(a, b);
        for (int i : commonElements) {
            System.out.println(i);

        }

        System.out.println("remove duplicates from array a");
        int[] removedDuplicates = analyzer.removeDuplicates(a);
        for (int i : removedDuplicates) {
            System.out.println(i);

        }

        System.out.println("Delete if more then twice repeat");
        int[] deleteMoreThenTwiceRepeat = analyzer.deleteMoreThenTwiceRepeat(a);
        for (int i : deleteMoreThenTwiceRepeat) {
            System.out.println(i);

        }

    }
}
